Name: memcache-top
Version: 0.6
Release: 2
Summary: 'perl command-line memcached stat reporter, top-like output'
License: BSD New License
Requires: perl
#Source0: https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/memcache-top/memcache-top-v0.6
%define __brp_mangle_shebangs %{nil}
%define debug_package %{nil}
%define srcdir %{_builddir}
%define pkgname memcache-top

%prep
 

%description
'perl command-line memcached stat reporter, top-like output'

%install
    export pkgdir="%{buildroot}"
    export srcdir="%{srcdir}"
    export pkgname="%{pkgname}"
    export _pkgname="%{_pkgname}"
    export _pkgfolder="%{_pkgfolder}"
    export pkgver="%{version}"  
    cd "%{_builddir}"               
    install -D -m755 "$srcdir/$pkgname-v$pkgver" "$pkgdir/usr/bin/$pkgname"

%files
/*
%exclude %dir /usr/bin
%exclude %dir /usr/lib

